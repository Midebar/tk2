from django.urls import path
from . import views

urlpatterns = [
    path('',views.search),
    path('result/',views.result, name='result'),
    path('indoSearch/',views.searchIndo, name='Search Indo')
]