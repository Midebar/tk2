from django import forms
from statistikCovid.models import Search

class SearchForm(forms.ModelForm):
    class Meta:
        model = Search
        fields = [
            'search'
            ]