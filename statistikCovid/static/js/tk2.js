$(document).ready(function(){
    $.ajax({
        url:'/statistik/indoSearch/',
        success: function(data){
            let searchResult='';
            searchResult +="<p class='display-4  d-flex justify-content-center kasus'> Kasus Aktif : " + data[0].positif + " orang </p><br>"+
            "<p class='display-4  d-flex justify-content-center kasus'> Kasus Meninggal : " + data[0].meninggal +" orang </p><br>"+
            "<p class='display-4 d-flex justify-content-center kasus'> Kasus Sembuh : "+ data[0].sembuh +" orang </p>"
        $('#resultIndo').html(searchResult);
        }
    })
})

$(".searchBtn").click(function(e){
    e.preventDefault();
    $.redirect("/statistik/result/",
	  {
	    search: $("#id_search").val(),
	  },
	  "GET");
})

$(".searchBtn").keypress(function(e){
    e.preventDefault();
    if (e.which == 13) {
        $.redirect("/statistik/result/",
	  {
	    search: $("#id_search").val(),
	  },
	  "GET");
    }
})