from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, Http404

from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib import messages

# Create your views here.

def index(request):
    return render(request, 'home/index.html')

def berita(request):
    return render(request, 'home/berita.html')

def logoutUser(request):
    logout(request)
    return redirect('landingpage:index')

def loginPage(request):
    if request.user.is_authenticated:
        return redirect('landingpage:index')

    else:
        if request.method == "POST":
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('landingpage:index')
            
            else:
                messages.info(request, 'Nama anda atau password anda salah...')
            
    context = {}
    return render(request, 'home/login.html', context)

def registerPage(request):
    if request.user.is_authenticated:
        return redirect('landingpage:index')

    else:    
        form = CreateUserForm()
        if request.method =="POST":
            form = CreateUserForm(request.POST)
            if form.is_valid():
                form.save()
                user = form.cleaned_data.get('username')
                messages.success(request, "Akun " + user + " sudah dibuat")
                return redirect('landingpage:login')

    context = {'form':form}
    return render(request, 'home/register.html', context)

