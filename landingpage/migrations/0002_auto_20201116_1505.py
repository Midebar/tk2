# Generated by Django 3.1.2 on 2020-11-16 08:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('landingpage', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='langganan',
            name='noTelepon',
            field=models.CharField(max_length=30),
        ),
    ]
