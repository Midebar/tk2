from django.urls import path
from . import views


app_name = 'landingpage'

urlpatterns = [
    path('', views.index, name='index'),
    path('berita/',views.berita, name= 'berita'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser, name='logout'),
    path('register/', views.registerPage, name='register'),

]
