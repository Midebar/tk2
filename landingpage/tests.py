from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest, response
from django.contrib.auth.models import User
from django.apps import apps
from landingpage.apps import LandingpageConfig


# Create your tests here.

class Landing_Page(TestCase):
    def test_index_is_exist(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
    
    def test_berita_is_exist(self):
        response = Client().get('/berita/')
        self.assertEquals(response.status_code, 200)

    def test_login_is_exist(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)

    def test_register_is_exist(self):
        response = Client().get('/register/')
        self.assertEquals(response.status_code, 200)

    def test_logOut(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)
    
    def test_LogoutExist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)


    def test_testRegister(self):
        response = self.client.post('/register/', data={'username':'mahasiswa123','email':'mahasiswa123@gmail', 'password1':'mahasiswa123' , 'password2':'mahasiswa123'})
        self.assertEqual(response.status_code, 200)

    def test_testLogin(self):
        response = self.client.post('/login/', data={'username':'mahasiswa123','password1':'mahasiswa123' , 'password2':'mahasiswa123'})
        self.assertEqual(response.status_code, 200)

    def test_testRegister1(self):
        user = User.objects.create_user(username='testuser', password="password")
        self.client.login(username="testuser", password="password")

        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_testRegister2(self):
        user = User.objects.create_user(username='testuser', password="password")
        self.client.login(username="testuser", password="password")

        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)
    
    def test_testRegisterExist(self):
        response = self.client.post('/register/', data={'username':'mahasiswa123','password1':'mahasiswa123' , 'password2':'mahasiswa123'})
        response = self.client.post('/register/', data={'username':'mahasiswa123','password1':'mahasiswa123' , 'password2':'mahasiswa123'})
        self.assertEqual(response.status_code, 200)

    def test_list_url_resolved_update(self):
        response = self.client.post('/login/', data={'username':'mahasiswa123','password1':'mahasiswa123' , 'password2':'mahasiswa123'})
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

        





