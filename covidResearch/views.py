from django.shortcuts import render, redirect
from django.forms import modelformset_factory
from covidResearch.forms import NewsForm, ImageForm, SearchForm
from covidResearch.models import News, Image
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import JsonResponse
import requests
import json

def home(request):
    dbNews = News.objects.all()
    apiKey = "babe11bf17544f5297874cef09b74d97"
    apiNews = requests.get("http://newsapi.org/v2/everything?q=covid&apiKey="+apiKey)
    return render(request, "home.html", {'News': dbNews, 'apiNews': apiNews})

@login_required
def uploadNews(request):
    ImageFormSet = modelformset_factory(Image, form=ImageForm, extra=5)
    if request.method == 'POST':
        formPost = NewsForm(request.POST, request.user)
        # Using Form Set for Multiple Files
        formSet = ImageFormSet(request.POST, request.FILES, queryset=Image.objects.none())
        formPost.author = request.user
        if formPost.is_valid() and formSet.is_valid():
            # Saved the form to render a model
            post_form = formPost.save(commit=False)
            post_form.save()
            for form in formSet.cleaned_data:
                # This helps to not crash if the user do not upload all the photos
                if form:
                    image = form['image']
                    photo = Image(news=post_form, image=image)
                    photo.save()
            return redirect("covidResearch:Home")
    else:
        form = NewsForm()
        formImageSet = ImageFormSet(queryset=Image.objects.none())
        return render(request, "upload_news.html", {'Form': form, 'ImageSet': formImageSet})

def detailNews(request, pk):
    singleNews = News.objects.get(pk = pk)
    return render(request, "detail_news.html", {'News': singleNews})


def searchNews(request):        
    searchForm = SearchForm()
    return render(request, "search_news.html", {"SearchForm" :searchForm})

def result(request):
    searchName =  request.GET.get('search')
    # Using Django Q method for query search. icontains for insensitive case
    dbNews = Q(title__icontains=searchName) | Q(author__icontains=searchName) | Q(shortDesc__icontains=searchName)
    dbResults = News.objects.filter(dbNews).distinct()
    apiKey = "babe11bf17544f5297874cef09b74d97"
    apiNews = requests.get("http://newsapi.org/v2/everything?q="+request.GET.get('search')+"&apiKey="+apiKey)
    apiResults = json.loads(apiNews.content)
    return JsonResponse(apiResults, safe=False)