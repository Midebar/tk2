from django.test import TestCase, Client
from django.apps import apps
from django.contrib.auth import get_user_model
from covidResearch.apps import CovidResearch
from covidResearch.models import News
import requests

class TestCovidResearch(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user(username='temporary', password='123')

    def test_url_home_exist(self):
        response = Client().get('/covidResearch/')
        self.assertEquals(response.status_code, 200)
  
    def test_url_search_exist(self):
        response = Client().get('/covidResearch/search/')
        self.assertEquals(response.status_code, 200)

    def test_url_apis_exist(self):
        response = requests.get("http://newsapi.org/v2/everything?q=covid&apiKey=babe11bf17544f5297874cef09b74d97")
        self.assertEquals(response.status_code, 200)
    
    def test_app_name(self):
        self.assertEqual(CovidResearch.name, 'covidResearch')
        self.assertEqual(apps.get_app_config('covidResearch').name, 'covidResearch')
    
    def test_object_is_created_and_rendered(self):
        #News attr = title, author, desc, shortDesc. date_published and image is not required
        News.objects.create(title="Bentuk Virus Covid-19", author="Burhan",
        desc="Covid-19 merupakan virus dari famili coronavidae yang berbentuk seperti mahkota ", shortDesc="Penjelasan dan Sejarah Covid-19")
        response = Client().get('/covidResearch/')
        self.assertContains(response, "Covid")
        response = Client().get('/covidResearch/news/1')
        self.assertContains(response, "Burhan")
        
    def test_login_work(self):
        User = get_user_model()
        login = Client().login(username='temporary', password='123')
        self.assertTrue(login)


    