$(document).ready(function(){
    $("#id_search").keyup(function(){
        var search = $("#id_search").val();
        $.ajax({
            url:'/covidResearch/result/?search='+search,
            success: function(apiNews){
                var array_items =  apiNews.articles;
                $("#api_search_result").find("tbody").empty();
                for(item in apiNews.articles){
                    var title = array_items[item].title;
                    var picture = array_items[item].urlToImage;
                    var desc = array_items[item].description;
                    var author = array_items[item].title;
                    var url = array_items[item].url;
                    $("#api_search_result").find("tbody").append("<tr><td><img src="+picture+" class="+"thumbnail"+"></img></td><td>"+title+"</td>"+"<td>"+author
                    +"</td><td>"+desc+"</td><td><a href="+url+"a>"+url+"</a></td></tr>");
                }
            }
        })
    })    
});
